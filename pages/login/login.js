import {
  getUserCode,
  userLogin,
  updateTokenTime
} from '../../api/login.js'
import Toast from '@vant/weapp/lib/toast/toast';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
  },
  goBack(){
    wx.navigateBack()
  },
  login() {
    Toast.loading({
      message: '加载中...',
      forbidClick: true,
    });
    
    let that = this
    wx.login({
      success: (res) => {
        getUserCode(res.code).then((data) => {
          userLogin(data.openid).then((result) => {
            that.setData({
              userInfo: result
            })
            wx.setStorageSync('userInfo', result)
            
            setTimeout(function () {
              wx.navigateBack({
                delta: 0,
              })
            }, 500)
          })
        }).catch((error)=>{
          Toast.fail('服务器异常');
        })
      },
    })
  },
 

  // updateTokenTime(){
  //   let userInfo =  wx.getStorageSync('userInfo')
  //   if (userInfo == null || userInfo == undefined || userInfo == ''){
  //     console.log("请登录！")
  //   }else{
  //     updateTokenTime(userInfo.token).then((res) => {
  //       let userInfoToken = wx.getStorageSync('userInfo')
  //       userInfoToken.token = res
  //       console.log(userInfoToken)
  //       wx.removeStorageSync('userInfo')
  //       wx.setStorageSync('userInfo', userInfoToken)
  //       console.log(res)
  //       if (res == "offline") {
  //         wx.removeStorageSync('userInfo')
  //       }
  //     })
  //   }
  // }
})