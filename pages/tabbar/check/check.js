const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    windowHeight: undefined, //屏幕高度
    windowWidth: undefined, //屏幕宽度
    margintop: "",
    funcrowheight: "",
    avatarUrl: "/images/nologin.png",
    nickName: 'Please Login'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
     // 定义功能菜单行与胶囊对其
    const {
      height,
      top
    } = wx.getMenuButtonBoundingClientRect();
    this.setData({
      windowHeight: app.globalData.windowHeight,
      windowWidth: app.globalData.windowWidth,
      margintop: top,
      funcrowheight: height
    })
  },
  login(){
    if (wx.getStorageSync('userInfo') != '') {
      wx.switchTab({
        url: '/pages/tabbar/mine/mine',
      })
      return
    }
    wx.navigateTo({
      url: '/pages/login/login',
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    console.log("111")
    console.log(wx.getStorageSync('userInfo'))
    let info = wx.getStorageSync('userInfo')
    if (info != ''){
        this.setData({
          avatarUrl: info.avatarUrl,
          nickName: info.nickName
        })
    }else{
      this.setData({
        avatarUrl: '/images/nologin.png',
        nickName: 'Please Login'
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  goMyCkeck(){
    if (wx.getStorageSync('userInfo') == '') {
      wx.showModal({
        content: '你还没有登录，是否去登录？',
        success: function (res) {
          if (res.confirm) {
            wx.navigateTo({
              url: '/pages/login/login',
            })
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
      return
    }
    wx.navigateTo({
      url: '/pages/tabbar/check/my-check/my-check',
    })
  },
  goUploadCkeck(){
    if (wx.getStorageSync('userInfo') == '') {
      wx.showModal({
        content: '你还没有登录，是否去登录？',
        success: function (res) {
          if (res.confirm) {
            wx.navigateTo({
              url: '/pages/login/login',
            })
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
      return
    }
    wx.navigateTo({
      url: '/pages/tabbar/check/upload-check/upload-check',
    })
  },
  goShareCkeck(){
    if (wx.getStorageSync('userInfo') == '') {
      wx.showModal({
        content: '你还没有登录，是否去登录？',
        success: function (res) {
          if (res.confirm) {
            wx.navigateTo({
              url: '/pages/login/login',
            })
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
      return
    }
    wx.navigateTo({
      url: '/pages/tabbar/check/share-check/share-check',
    })
  },
  goNearbyHospital(){
    wx.switchTab({
      url: '/pages/tabbar/hospital/hospital',
    })
  },
})