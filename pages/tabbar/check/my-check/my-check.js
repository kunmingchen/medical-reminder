const app = getApp()
Page({
  data: {
    option1: [
      { text: '默认排序', value: 'a' },
      { text: '按时间降序排序', value: 'b' },
      { text: '按时间升序排序', value: 'c' },
      { text: '按费用降序排序', value: 'd' },
      { text: '按费用升序排序', value: 'e' },
    ],
    value1: 'a',
    windowHeight: undefined, //屏幕高度
    windowWidth: undefined, //屏幕宽度
    margintop: "",
    funcrowheight: "",
    tabbarHeight: undefined, //tabbar高度
    safeBottomHeight: undefined,//苹果底部横条高度
    topHeight: undefined,
  },
  onLoad(options){
    const {
      height,
      top
    } = wx.getMenuButtonBoundingClientRect();
    this.setData({
      windowHeight: app.globalData.windowHeight,
      windowWidth: app.globalData.windowWidth,
      tabbarHeight: app.globalData.tabbarHeight,
      safeBottomHeight: app.globalData.safeBottomHeight,
      topHeight: app.globalData.topHeight,
      margintop: top,
      funcrowheight: height
    })
  },
  previewImages(){
    wx.previewImage({
      current: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fsafe-img.xhscdn.com%2Fbw1%2F84b454cb-262f-4b45-958d-a8103d204430%3FimageView2%2F2%2Fw%2F1080%2Fformat%2Fjpg&refer=http%3A%2F%2Fsafe-img.xhscdn.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1704266162&t=d040e7b2ab8ee5e48f6e693aa5e3303d', // 当前显示图片的链接/路径
      urls: [
        'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fsafe-img.xhscdn.com%2Fbw1%2F84b454cb-262f-4b45-958d-a8103d204430%3FimageView2%2F2%2Fw%2F1080%2Fformat%2Fjpg&refer=http%3A%2F%2Fsafe-img.xhscdn.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1704266162&t=d040e7b2ab8ee5e48f6e693aa5e3303d',
        '/images/yiyuanimage.png',
        '/images/yiyuanimage.png'
      ] // 需要预览的图片链接/路径列表
    });

  },
})