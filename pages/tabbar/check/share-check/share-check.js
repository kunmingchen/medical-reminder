const app = getApp()
Page({
  data: {
    option1: [
      { text: '默认排序', value: 'a' },
      { text: '按时间降序排序', value: 'b' },
      { text: '按时间升序排序', value: 'c' },
      { text: '按费用降序排序', value: 'd' },
      { text: '按费用升序排序', value: 'e' },
    ],
    value1: 'a',
    windowHeight: undefined, //屏幕高度
    windowWidth: undefined, //屏幕宽度
    margintop: "",
    funcrowheight: "",
    tabbarHeight: undefined, //tabbar高度
    safeBottomHeight: undefined,//苹果底部横条高度
    topHeight: undefined,
    ckm: '', //查看码
    neirong: 'none',
  },
  onLoad(options){
    const {
      height,
      top
    } = wx.getMenuButtonBoundingClientRect();
    this.setData({
      windowHeight: app.globalData.windowHeight,
      windowWidth: app.globalData.windowWidth,
      tabbarHeight: app.globalData.tabbarHeight,
      safeBottomHeight: app.globalData.safeBottomHeight,
      topHeight: app.globalData.topHeight,
      margintop: top,
      funcrowheight: height
    })
  },
  clearInput(){
    this.setData({neirong: 'none'})
  },
  changeInput(e){
    this.setData({ckm:e.detail})
    if (this.data.ckm == '') {
      this.setData({neirong: 'none'})
    }
    
  },
  chakan(){
    console.log(this.data.ckm)
    if (this.data.ckm != '') {
      this.setData({neirong: 'block'})
    }
  },
})