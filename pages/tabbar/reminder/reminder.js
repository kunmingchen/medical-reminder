const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    windowHeight: undefined, //屏幕高度
    windowWidth: undefined, //屏幕宽度
    calendarHeight: 140,
    margintop: "",
    funcrowheight: "",
    cyChecked: false,
    fcChecked: false,
    currentTime: '', // 用于存储当前时间的数据
    showSheet: false,
    actions: [{
        name: '选项',
      },
      {
        name: '选项',
      },
      {
        name: '选项',
        subname: '描述信息',
        openType: 'share',
      },
    ],
    userInfo: ''
  },
  onChangeCy({detail}) {
    if (wx.getStorageSync('userInfo') == '') {
      console.log("你没有登录")
      wx.showModal({
        content: '你还没有登录，是否去登录？',
        success: function (res) {
          if (res.confirm) {
            wx.navigateTo({
              url: '/pages/login/login',
            })
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
      return
    }
    if (detail) {
      wx.showModal({
        title: '吃药提醒',
        content: '是否开启微信提醒？',
        success: (res) => {
          if (res.confirm) {
            this.setData({
              cyChecked: detail
            });
          }
        },
      });
    } else {
      this.setData({
        cyChecked: false
      });
    }
  },
  onChangeFc({detail}) {
    if (wx.getStorageSync('userInfo') == '') {
      console.log("你没有登录")
      wx.showModal({
        content: '你还没有登录，是否去登录？',
        success: function (res) {
          if (res.confirm) {
            wx.navigateTo({
              url: '/pages/login/login',
            })
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
      return
    }
    if (detail) {
      wx.showModal({
        title: '复查提醒',
        content: '是否开启微信提醒？',
        success: (res) => {
          if (res.confirm) {
            this.setData({
              fcChecked: detail
            });
          }
        },
      });
    } else {
      this.setData({
        fcChecked: false
      });
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    const {
      height,
      top
    } = wx.getMenuButtonBoundingClientRect();
    this.setData({
      windowHeight: app.globalData.windowHeight,
      windowWidth: app.globalData.windowWidth,
      margintop: top,
      funcrowheight: height
    })
    // 使用 setInterval 函数每秒更新一次时间
    setInterval(() => {
      this.setData({
        currentTime: this.getNowTime()
      });
    }, 1000);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    wx.requestSubscribeMessage({
      tmplIds: ['1', '2', '3'], // 订阅消息模板ID 最多只能提醒三个模版消息
      success(res) {
        console.log("授权完成", res);
      },
      complete() {
        console.log('接口调用结束的回调函数（调用成功、失败都会执行）');
      },
      fail(err) {
        console.log('失败了', err);
      }

    })
  },

  getHeight(e) {
    this.setData({
      calendarHeight: e.detail.height
    })
  },
  setting() {
    if (wx.getStorageSync('userInfo') == '') {
      console.log("你没有登录")
      wx.showModal({
        content: '你还没有登录，是否去登录？',
        success: function (res) {
          if (res.confirm) {
            wx.navigateTo({
              url: '/pages/login/login',
            })
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
      return
    }
    wx.showActionSheet({
      itemList: ['设置吃药提醒', '设置复查提醒'],
      success: function (res) {
        if (!res.cancel) {
          if (res.tapIndex == 0) {
            wx.navigateTo({
              url: '/pages/tabbar/reminder/setting-medicine-reminder/setting-medicine-reminder',
            })
          } else if (res.tapIndex == 1) {
            wx.navigateTo({
              url: '/pages/tabbar/reminder/setting-check-reminder/setting-check-reminder',
            })
          }
        }
      }
    })

  },

  // 获取当前时间的函数
  getNowTime: function () {
    const date = new Date();
    const year = date.getFullYear();
    const month = this.formatNumber(date.getMonth() + 1);
    const day = this.formatNumber(date.getDate());
    const hour = this.formatNumber(date.getHours());
    const minute = this.formatNumber(date.getMinutes());
    const second = this.formatNumber(date.getSeconds());
    return `${year}-${month}-${day} ${hour}:${minute}:${second}`;
  },

  // 格式化数字，补零操作
  formatNumber: function (n) {
    n = n.toString();
    return n[1] ? n : '0' + n;
  },


  onShow() {
    this.setData({
      userInfo: wx.getStorageSync('userInfo')
    })
    console.log("userinfo:")
    console.log(wx.getStorageSync('userInfo'))
    console.log(this.data.userInfo)
  },

})