import {
  getUserCheckInfo,
  setCheckInfo
} from '../../../../api/reminder/reminder.js'
import Toast from '@vant/weapp/lib/toast/toast';
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    windowHeight: undefined, //屏幕高度
    windowWidth: undefined, //屏幕宽度
    margintop: "",
    funcrowheight: "",
    tabbarHeight: undefined, //tabbar高度
    safeBottomHeight: undefined,//苹果底部横条高度
    topHeight: undefined,
    showCalendar: false,
    //选择的日期
    calendarText: '',
    //复查间隔（月）
    interval: '',
    //是否开启自动提醒 0否 1是
    fcChecked: false,
    minDate: new Date(2023, 0, 1).getTime(),
    userInfo: {},
    defaultDate: null,//默认选中日期

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    const {
      height,
      top
    } = wx.getMenuButtonBoundingClientRect();
    this.setData({
      windowHeight: app.globalData.windowHeight,
      windowWidth: app.globalData.windowWidth,
      tabbarHeight: app.globalData.tabbarHeight,
      safeBottomHeight: app.globalData.safeBottomHeight,
      topHeight: app.globalData.topHeight,
      margintop: top,
      funcrowheight: height,
      userInfo: wx.getStorageSync('userInfo')
    })

    
    getUserCheckInfo(this.data.userInfo.token).then((res)=>{
      console.log(res)
      this.setData({
        calendarText: res.lastTimeCheck,
        defaultDate: Date.parse(res.lastTimeCheck),
        interval: res.interval / 30,
        fcChecked: res.isOpen == 0?false:true
      })  
    })

  },

  showCalendarFunc() {
    this.setData({
      showCalendar: true
    });
  },
  onClose() {
    this.setData({
      showCalendar: false
    });
  },
  onConfirm(event) {
    console.log(event)
    this.setData({
      showCalendar: false,
      calendarText: this.formatDate(event.detail)
    });
  },
  onChangeFc({detail}) {
    if (detail) {
      wx.showModal({
        title: '复查提醒',
        content: '是否开启微信提醒？',
        success: (res) => {
          if (res.confirm) {
            this.setData({
              fcChecked: detail
            });
          }
        },
      });
    } else {
      this.setData({
        fcChecked: false
      });
    }
  },
  bindInput(e){
    this.setData({interval: e.detail.value})
  },

  saveCheckInfo(){
    if (this.data.interval == '') {
      Toast.fail('请输入间隔');
      return
    }
    if (this.data.calendarText == '') {
      Toast.fail('请选择日期');
      return
    }
    let checkInfo = {
      token: this.data.userInfo.token,
      interval: this.data.interval * 30,
      lastTimeCheck: this.data.calendarText,
      isOpen: this.data.fcChecked==false?0:1
    }
    setCheckInfo(checkInfo).then((res)=>{
      if(res){
        Toast.success('设置成功');
        setTimeout(()=>{
          wx.navigateBack()
        },2000)
      }else{
        Toast.fail('Error!');
      }
    })
  },



  formatDate(date) {
    date = new Date(date);
    const year = date.getFullYear();
    const month = (date.getMonth() + 1).toString().padStart(2, '0');
    const day = date.getDate().toString().padStart(2, '0');
    return `${year}-${month}-${day}`;
  }
})