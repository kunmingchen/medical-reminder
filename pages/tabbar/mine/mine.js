const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    windowHeight: undefined, //屏幕高度
    windowWidth: undefined, //屏幕宽度
    margintop: "",
    funcrowheight: "",
    avatarUrl: "/images/nologin.png",
    nickName: "授权登录"
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
     // 定义功能菜单行与胶囊对其
    const {
      height,
      top
    } = wx.getMenuButtonBoundingClientRect();
    this.setData({
      windowHeight: app.globalData.windowHeight,
      windowWidth: app.globalData.windowWidth,
      margintop: top,
      funcrowheight: height
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    console.log("hhh?")

    let info = wx.getStorageSync('userInfo')
    console.log(info)
    if (info != ''){
        this.setData({
          avatarUrl: info.avatarUrl,
          nickName: info.nickName
        })
    }else{
      this.setData({
        avatarUrl: '/images/nologin.png',
        nickName: '授权登录'
      })
    }

    console.log(this.data.nickName)
  },
  login(){
    if (wx.getStorageSync('userInfo') != '') {
      wx.switchTab({
        url: '/pages/tabbar/mine/mine',
      })
      return
    }
    wx.navigateTo({
      url: '/pages/login/login',
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  reminderTap(){
    wx.switchTab({
      url: '/pages/tabbar/reminder/reminder',
    })
  },
  settingTap(){
    wx.navigateTo({
      url: '/pages/tabbar/mine/setting/setting',
    })
  },
  feedbackTap(){
    wx.navigateTo({
      url: '/pages/tabbar/mine/feedback/feedback',
    })
  },

})