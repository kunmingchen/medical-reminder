import request from "../../utils/request";

export function getUserDrugInfo(token) {
  return request({
    url: '/drug/getUserDrugInfo',
    data:{token}
  });
}

export function setDrugInfo(data) {
  return request({
    url: '/drug/setDrugInfo',
    method: 'post',
    data:data
  });
}