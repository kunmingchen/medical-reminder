import request from "../../utils/request";

export function getUserCheckInfo(token) {
  return request({
    url: '/check/getUserCheckInfo',
    data:{token}
  });
}

export function setCheckInfo(data) {
  return request({
    url: '/check/setCheckInfo',
    method: 'post',
    data:data
  });
}