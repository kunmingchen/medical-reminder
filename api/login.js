import request from "../utils/onlineRequest";


export function getUserCode(code) {
  return request({
    url: '/user/getUserCode',
    data:{code}
  });
}
export function userLogin(openId) {
  return request({
    url: '/user/userLogin',
    data:{openId}
  });
}

export function updateTokenTime(token) {
  return request({
    url: '/user/updateTokenTime',
    data:{token}
  });
}