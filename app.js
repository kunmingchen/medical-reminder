// app.js
App({
  onLaunch() {
    let that = this
    //获取右上角胶囊按钮（即菜单按钮）的布局位置信息，包括它的位置、尺寸等
    const rect = wx.getMenuButtonBoundingClientRect()
    console.log(rect)
    //获取设备系统信息
    wx.getSystemInfo({
      success: (res) => {
        console.log(res)
        // 获取导航栏高度
        let navigationBarHeight = res.statusBarHeight + res.navigationBarHeight - res.statusBarHeight * 2;
        console.log(navigationBarHeight);
        that.globalData.screenHeight = res.screenHeight;
        that.globalData.screenWidth = res.screenWidth;
        that.globalData.windowHeight = res.windowHeight;
        that.globalData.windowWidth = res.windowWidth;
        that.globalData.safeBottomHeight = res.screenHeight - res.safeArea.bottom
        that.globalData.topHeight = rect.top + rect.height + 8
      }
    })
  },
  globalData: {
    screenHeight: undefined, //设备高度
    screenWidth: undefined, //设备宽度
    windowHeight: undefined, //屏幕高度
    windowWidth: undefined, //屏幕宽度
    tabbarHeight: 44, //tabbar高度
    safeBottomHeight: undefined,//苹果底部横条高度
    topHeight: undefined, //顶部高度，没有设置custom需减去
  }
})
