//不经过token验证
function onlineRequest(options) {
  // const API_ROOT = "http://192.168.1.2:8088";
  const API_ROOT = "http://192.168.2.50:8088";
  return new Promise((resolve, reject) => {
    wx.request({
      ...options,
      url: API_ROOT + options.url, // 在请求的 URL 前加上 API_ROOT
      success: function (res) {
        if (res.statusCode === 200) {
          resolve(res.data);
        } else {
          reject(res.errMsg);
        }
      },
      fail: function (err) {
        reject(err.errMsg);
      },
    });
  });
}
export default onlineRequest;
